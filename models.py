from peewee import SqliteDatabase, Model, ForeignKeyField,\
    IntegerField, TextField


db = SqliteDatabase('reciepe.db')

class BaseModel(Model):
    class Meta:
        database = db
        
class Serving(BaseModel):
    amount = TextField()
    title = TextField()

class Directions(BaseModel):
    order = IntegerField()
    directions = TextField()

    class Meta:
        order_by = ('order',)

class Difficulty(BaseModel):
    title = TextField()
    
class Reciepe(BaseModel):
    serving = ForeignKeyField(Serving)
    directions = ForeignKeyField(Directions)
    difficulty = ForeignKeyField(Difficulty)
    prep_time = TextField()