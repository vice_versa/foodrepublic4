# coding: utf-8

import grab
import logging

from grab.spider import Spider, Task
from models import Reciepe 

class FoodRepublicSpider(Spider):
    initial_urls = ['http://www.foodrepublic.com/recipes']
    base_url = "http://www.foodrepublic.com/"

    def prepare(self):
        pass

    def task_initial(self, grab, task):
        for elem in grab.xpath_list('//div[@class="recipe-link"]/a')[:5]:
            yield Task('reciepe', url=elem.get('href'))

    def task_reciepe(self, grab, task):
        serving = grab.xpath_text('//div[@class="fieldgroup group-recipe-serving-size"]/div/span')
        #Reciepe.create(serving=serving)
        
        ingredient_list = grab.xpath_list('//div[@class="ingredient recipe-ingredient"]')
        for ingredient in ingredient_list:
            amount = ingredient.find('span[@class="amount"]').text
            name = ingredient.find('span[@class="name"]').text
            print amount, name
        
        instruction_list = grab.xpath_list('//div[@class="field-item odd"]/ol/li')
        for i, elem in enumerate(instruction_list):
            print i, elem.text
                                           
        difficulty = grab.xpath_text("""//div[@class="field field-type-text field-field-recipe-difficulty"]
                                      /div[@class="field-items"]
                                      /div[@class="field-item odd"]""")
                                      
        
        print difficulty
        
        prep_time = grab.xpath("""//div[@class="field field-type-text field-field-recipe-prep-time"]
                                /div[@class="field-items"]
                                /div[@class="field-item odd"]
                                /span/span""").get("title")
        print prep_time              
        
if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG)
    bot = FoodRepublicSpider(thread_number=2)
    bot.run()